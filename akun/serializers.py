from rest_framework import serializers
from .models import Akun

class AkunSerializer(serializers.ModelSerializer):
    class Meta:
        model = Akun
        fields = (
            "username",
            "password",
            "fullname",
            "npm",
            "client_id",
            "client_secret",
            "token",
            "created_at",
            "refresh_token"
        )
        depth = 1
