from rest_framework.response import Response
from rest_framework import status

errorToken = Response(
    {
        "error": "invalid_request",
        "Error_description": "ada kesalahan masbro!"
    },
    status=status.HTTP_401_UNAUTHORIZED
)

errorResource = Response(
    {
        "error": "invalid_request",
        "Error_description": "Token salah masbro!"
    },
    status=status.HTTP_401_UNAUTHORIZED
)

def createTokenResponse(access_token, refresh_token, expire_time):

    return Response(
        {
            "access_token": access_token,
            "expires_in": expire_time,
            "token_type": "Bearer",
            "scope": None,
            "refresh_token": refresh_token
        },
        status=status.HTTP_200_OK
    )

def createResourceResponse(
    access_token, 
    client_id, 
    user_id,
    full_name,
    npm,
    refresh_token, 
    expire_time
):
    return Response(
        {
            "access_token" : access_token,
            "client_id" : client_id,
            "user_id" : user_id,
            "full_name" : full_name,
            "npm" : npm,
            "expires" : expire_time,
            "refresh_token" : refresh_token
        },
        status=status.HTTP_200_OK
    )

def createUserResponse(
     client_id, 
    username,
    full_name,
    npm,
): 
     return Response(
        {
            "client_id" : client_id,
            "username" : username,
            "full_name" : full_name,
            "npm" : npm,
        },
        status=status.HTTP_200_OK
    )