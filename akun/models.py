from django.db import models

# Create your models here.
class Akun(models.Model):
    username= models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    fullname= models.CharField(max_length=100, null = True)
    npm = models.CharField(max_length=10, null = True)
    client_id = models.CharField(max_length=100)
    client_secret = models.CharField(max_length=100)
    token = models.CharField(max_length=40, null = True, blank = True)
    created_at = models.DateTimeField(null = True, blank = True)
    end_at = models.DateTimeField(null = True, blank = True)
    refresh_token = models.CharField(max_length=40, null = True, blank = True)

    class Meta:
        unique_together = (('username', 'client_id'),)


