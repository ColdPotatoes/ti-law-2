from datetime import datetime
from datetime import timedelta


time_str = '23/2/2020 11:12:22.234513'
date_format_str = '%d/%m/%Y %H:%M:%S.%f'

def addMinutes(targetTime, minutes: int):
    targetTime = targetTime.timestamp()
    final_time = targetTime + 300

    return datetime.fromtimestamp(final_time)