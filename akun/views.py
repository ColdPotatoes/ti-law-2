from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .serializers import AkunSerializer
from .models import Akun
from .response import errorToken, errorResource, createTokenResponse, createResourceResponse, createUserResponse
from .datetimeutil import addMinutes
import string
import random
import math
from django.utils import timezone
# Create your views here.

def id_generator(size=40, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

@api_view(['POST'])
def requestToken(request):
    data = request.data
    expireTime = 300
    currentTime= timezone.now()

    try :
        oldAkun = Akun.objects.get(username=data['username'], client_id=data['client_id'])
    except Akun.DoesNotExist:
        return errorToken

    # check if password and secret is correct
    if(data['password'] != oldAkun.password or data['client_secret'] != oldAkun.client_secret):
        return errorToken
    
    #first login
    if(oldAkun.created_at == None):
        oldAkun.token = id_generator()
        oldAkun.created_at = currentTime
        oldAkun.end_at = addMinutes(currentTime, 5)
        oldAkun.refresh_token = id_generator()
    else:
        #token expired
        if(currentTime >= oldAkun.end_at):
            oldAkun.token = id_generator()
            oldAkun.created_at = currentTime
            oldAkun.end_at = addMinutes(currentTime, 5)

        #not expired
        else:
            expireTime = oldAkun.end_at.timestamp() - currentTime.timestamp()
            expireTime = round(expireTime)
            print(expireTime)

    oldAkun.save()
    serializedAkun = AkunSerializer(oldAkun).data

    return createTokenResponse(
        serializedAkun["token"], 
        serializedAkun["refresh_token"],
        expireTime
    )

@api_view(['POST'])
def requestResource(request):
    bearer_token = request.META.get('HTTP_AUTHORIZATION')
    list_token = bearer_token.split()
    bearer = list_token[0]
    token = list_token[1]
    currentTime= timezone.now()

    if(bearer != "Bearer"):
        return errorResource

    try :
        oldAkun = Akun.objects.get(token=token)
    except Akun.DoesNotExist:
        return errorResource

    expireTime = oldAkun.end_at.timestamp() - currentTime.timestamp()
    expireTime = round(expireTime)

    return createResourceResponse(
        oldAkun.token,
        oldAkun.client_id,
        oldAkun.username,
        oldAkun.fullname,
        oldAkun.npm,
        oldAkun.refresh_token,
        expireTime
    )

@api_view(['GET'])
def requestProfile(request):
    data = request.data
    
    try :
        oldAkun = Akun.objects.get(username=data['username'], client_id=data['client_id'])
    except Akun.DoesNotExist:
        return errorToken

    return createUserResponse(
        oldAkun.client_id,
        oldAkun.username,
        oldAkun.fullname,
        oldAkun.npm
    )
