from django.contrib import admin

from akun.models import Akun

# Register your models here.

class AkunAdmin(admin.ModelAdmin):
    list_display = (
        "username",
        "password",
        "fullname",
        "npm",
        "client_id",
        "client_secret",
        "token",
        "created_at",
        "refresh_token"
    )

admin.site.register(Akun, AkunAdmin)